﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;
    public GameObject playerBorder;
    public GameObject enemyBorder;
    public GameObject player;
    public GameObject gameOverScreen;
    public int playerFoodPoints = 180;
    public float turnDelay = .1f;
    [HideInInspector] public bool playersTurn = true;
    private List<Enemy> enemies;
    private bool enemiesMoving;
    void Awake()
    {
        if (instance == null)
            instance = this;
        enemies = new List<Enemy>();
    }

    public void GameOver()
    {
        gameOverScreen.SetActive(true);
        player.SetActive(false);
        enabled = false;
    }
    IEnumerator MoveEnemies()
    {
        enemiesMoving = true;
        yield return new WaitForSeconds(turnDelay);
        if (enemies.Count == 0)
        {
            yield return new WaitForSeconds(turnDelay);
        }
        for (int i = 0; i < enemies.Count; i++)
        {
            enemies[i].MoveEnemy();
            yield return new WaitForSeconds(enemies[i].moveTime);
        }
        playersTurn = true;
        enemiesMoving = false;
    }
    void Update()
    {
        if (playersTurn)
            playerBorder.SetActive(true);
        if (playersTurn == false)
            playerBorder.SetActive(false);
        if (enemiesMoving)
            enemyBorder.SetActive(true);
        if (enemiesMoving == false)
            enemyBorder.SetActive(false);
        if (playersTurn || enemiesMoving)
            return;
        StartCoroutine(MoveEnemies());
    }
    public void AddEnemyToList(Enemy script)
    {
        enemies.Add(script);
    }
}
